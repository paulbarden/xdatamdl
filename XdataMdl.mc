/*-----------------------------------------------------------------+
|                                                                  |
| MDL example to show creation & use of basic dialog box           |
|                                                                  |
| - - - - - - - - - - - - - - - - - - - - - - - - - - - - -        |
|                                                                  |
| Public Routine Summary -                                         |
|                                                                  | 
| main - Main entry point and initialization logic                 |
| basic_dialogHook - Dialog box hook function                      |
| basic_toggleButtonHook - Toggle button item hook function        |
| basic_openModal - Application command function to open modal     |
| dialog box                                                       |
| basic_errorPrint - Function to display an error message in       |    
| messages dialog box                                              |
|                                                                  |
+-----------------------------------------------------------------*/
/*-----------------------------------------------------------------+
|                                                                  |
| Include Files                                                    |
|                                                                  |
+-----------------------------------------------------------------*/
#include <mdl.h>            /* MDL Library funcs structures & constants */
#include <dlogitem.h>       /* Dialog Box Manager structures & constants */
#include <cexpr.h>          /* C Expression structures & constants */
#include <cmdlist.h>        /* MicroStation command numbers */
#include <dlogman.fdf>      /* dialog box manager function prototypes */
#include <msrsrc.fdf>
#include <mssystem.fdf>
#include <mscexpr.fdf>
#include <msparse.fdf>
#include <msdialog.fdf>
#include <mslocate.fdf>
#include <msstate.fdf>
#include <mselemen.fdf>
#include <mslinkge.fdf>
#include <msdwgappdata.fdf>
#include "XdataMdl.h"       /* basic dialog box example constants & structs */
#include "XdataMdlCmd.h"    /* basic dialog box command nums */
/*-----------------------------------------------------------------+
|                                                                  |
| Private Global variables                                         |
|                                                                  |
+-----------------------------------------------------------------*/
/* The following variable is referenced in C expression strings used
by the text, option button, and toggle button items defined in
basic.r. The initial external state of the text item and the option
button (they are both looking at the same application variable) is 1.
The initial external state of the toggle button in the modal dialog
box is 0 (the toggle is "out" or OFF) */

Private BasicGlobals basicGlobals={1, 0};

void basic_toggleButtonHook(DialogItemMessage *dimP);
void basic_dialogHook(DialogMessage *dmP);
void basic_errorPrint(int errorNumber);
void acceptElm(Dpoint3d *pntP, int view);


Private DialogHookInfo uHooks[]=
{
    {HOOKITEMID_ToggleButton_Basic, basic_toggleButtonHook},
    {HOOKDIALOGID_Basic, basic_dialogHook}
};
/*-----------------------------------------------------------------+
|                                                                  |
| name main                                                        |
|                                                                  |
| author BSI 12/90                                                 |
|                                                                  |
+-----------------------------------------------------------------*/
Public main
(
    int argc,               /* => number of args in next array */
    char *argv[]            /* => array of cmd line arguments */
)
{
    SymbolSet *setP;            /* a ptr to a "C expression symbol set" */
    RscFileHandle rscFileH;     /* a resource file handle */
    DialogBox *dbP;             /* a ptr to a dialog box */

    /* open the resource file that we came out of */
    mdlResource_openFile(&rscFileH, NULL, 0);

    /* load the command table */
    if (mdlParse_loadCommandTable(NULL) == NULL)
        basic_errorPrint(ERRID_CommandTable);

    /* set up variables to be evaluated w/i C expression strings */
    setP=mdlCExpression_initializeSet(VISIBILITY_DIALOG_BOX,0,FALSE);
    mdlDialog_publishComplexVariable(setP, "basicglobals", "basicGlobals", &basicGlobals);

    /* publish our hook functions */
    mdlDialog_hookPublish(sizeof(uHooks)/sizeof(DialogHookInfo),uHooks);

    /* open the "Basic" dialog box */
    if ((dbP = mdlDialog_open(NULL, DIALOGID_Basic)) == NULL)
        basic_errorPrint(ERRID_BasicDialog);
    return 1;
    }
/*----------------------------------------------------------------+
|                                                                 |
| Hook Functions                                                  |
|                                                                 |
+----------------------------------------------------------------*/
/*----------------------------------------------------------------+
|                                                                 |
| name basic_dialogHook                                           |
|                                                                 |
| author BSI 12/90                                                |
|                                                                 |
+----------------------------------------------------------------*/
Private void basic_dialogHook
(
    DialogMessage *dmP /* => a ptr to a dialog message */
)
{
    /* ignore any messages being sent to modal dialog hook */
    if (dmP->dialogId != DIALOGID_Basic)
        return;
    dmP->msgUnderstood = TRUE;
    switch (dmP->messageType)
    {
        case DIALOG_MESSAGE_DESTROY:
        {
            /* unload this mdl task when Basic Dialog is closed */
            mdlDialog_cmdNumberQueue(FALSE, CMD_MDL_UNLOAD,
            mdlSystem_getCurrTaskID(), TRUE);
            /* mdlSystem_getCurrTaskID was erroneously omitted
            from the MDL documentation. It returns a character
            pointer pointing to the current task's task ID. */
            break;
        };
        default:
            dmP->msgUnderstood = FALSE;
        break;
    }
}
/*-----------------------------------------------------------------+
|                                                                  |
| name basic_toggleButtonHook                                      |
|                                                                  |
| author BSI 12/90                                                 |
|                                                                  |
+-----------------------------------------------------------------*/
Private void basic_toggleButtonHook
(
    DialogItemMessage *dimP /* => a ptr to a dialog item message */
)
{
    dimP->msgUnderstood = TRUE;
    switch (dimP->messageType)
    {
        case DITEM_MESSAGE_CREATE:
        {
            basicGlobals.parameter2 = 0;
        break;
        }
        default:
            dimP->msgUnderstood = FALSE;
        break;
    }
}
/*-----------------------------------------------------------------+
|                                                                  |
| Command Handling routines                                        |
|                                                                  |
+-----------------------------------------------------------------*/
/*-----------------------------------------------------------------+
|                                                                  |
| Open Modal Dialog Command                                                         |
|                                                                  |
| author BSI 12/90                                                 |
|                                                                  |
+-----------------------------------------------------------------*/
Public cmdName void basic_openModal
(
    char *unparsedP /* => unparsed part of command */
) cmdNumber CMD_ASDXDATA_OPENMODAL
{
    int lastAction;
    /* open child modal dialog box */
    if (mdlDialog_openModal(&lastAction, NULL, DIALOGID_BasicModal))
    {
        basic_errorPrint (ERRID_ModalDialog);
        return;
    }
    /* if the user clicked in OK push button, and the toggle button
    is currently "in", then increment parameter1 and update the
    appearance of both the text item and the option button item
    in the Basic dialog box */
    if (lastAction == ACTIONBUTTON_OK && basicGlobals.parameter2)
    {
        basicGlobals.parameter1++;
        if (basicGlobals.parameter1 > 3)
            basicGlobals.parameter1 = 1;
        mdlDialog_synonymsSynch(NULL, SYNONYMID_Basic, NULL);
    }
}
/*-----------------------------------------------------------------+
|                                                                  |
| XData Show Command                                               |
|                                                                  |
| author ASD 03/21                                                 |
|                                                                  |
+-----------------------------------------------------------------*/

Public cmdName void xdata_show
(
    char *unparsedP /* => unparsed part of command */
) cmdNumber CMD_ASDXDATA_SHOW
{
    mdlDialog_dmsgsPrint("Select an element.");

    mdlLocate_normal(); /*Set locate mask to all dispayable elements */

    mdlState_startModifyCommand (
            xdata_show,                 /* reset func */
            acceptElm,                  /* datapoint func */
            NULL,                       /* dynamics func */
            NULL,                       /* show func */
            NULL,                       /* clean func */
            COMMANDID_XDataShow,        /* command field message */
            PROMPTID_IdentifyElement,   /* prompt message */
            TRUE,                       /* use selection sets */
            0);                         /* points required for accept */

    /* Start search at beginning of file */
    mdlLocate_init ();
}

Public void acceptElm(Dpoint3d *pntP, int view)
{
    ULong	           filePos;
    DgnModelRefP       currFileP;
    char               str[80];
    MSElementUnion     ele;
    byte*              pXData;
    UInt32             XDataSizeOut; 
    XDataValueUnion    XDataUnion;
    int                XDataValueType;
    UInt32             XDataSize; 
    int                XDataGroupCode;
    UInt32             XDataIndex;


    filePos = mdlElement_getFilePos(FILEPOS_CURRENT, &currFileP);

    mdlDialog_dmsgsClear();
    sprintf(str, "Got element %lu.", filePos);
    mdlDialog_dmsgsPrint(str);

    /* Now attempt to read the element we selected */
    if(mdlElement_read(&ele, currFileP, filePos ) != SUCCESS)
        mdlDialog_dmsgsPrint("Could not read element.");

    XDataIndex = 0;
    /* Does the element have the XData we need */
    if(mdlLinkage_extractXDataLinkage(&pXData, &XDataSizeOut, &ele) == SUCCESS)
    {
        while(mdlLinkage_getXDataGroupCode(&XDataUnion, &XDataValueType, &XDataSize, &XDataGroupCode, &ele, &XDataIndex)  == SUCCESS)
        {
            switch(XDataValueType)
            {
                case XDATAVALUE_Int16:
                    sprintf(str, "16 Bit Integer: %d\n", (&XDataUnion)->int16);
                    break;

                case XDATAVALUE_Int32:
                    sprintf(str, "32 Bit Integer: %d\n", (&XDataUnion)->int32);
                    break;

                case XDATAVALUE_Double:	
                    sprintf(str, "Double Precision: %f\n", (&XDataUnion)->doubleValue);
                    break;

                case XDATAVALUE_ElementId:
                    sprintf(str, "Element ID: %I64x\n", (&XDataUnion)->elementId);
                    break;

                case XDATAVALUE_Point:
                    sprintf(str, "Point: (%f, %f, %f)\n", (&XDataUnion)->point.x, (&XDataUnion)->point.y, (&XDataUnion)->point.z );
                    break;

                case XDATAVALUE_String:
                    sprintf(str, "String: %s\n", (&XDataUnion)->pString );
                    free((&XDataUnion)->pString);
                    break;

                case XDATAVALUE_Binary:
                    sprintf(str, "Binary Data, %d Bytes <not shown>\n", XDataSize );
                    free((&XDataUnion)->pBinaryData);
                    break;
               
                default:
                    sprintf(str, "Unknown group code %i\n", XDataGroupCode);
            }
            mdlDialog_dmsgsPrint(str);
        }
    }
    else
        mdlDialog_dmsgsPrint("Element has no XData.\n");

    /* Restart the locate logic */
    mdlLocate_restart (FALSE);
}
    
Public  void setElmSearchType()
{
    static int searchType[] = {LINE_ELM, TEXT_ELM,};

    /* Clear search criteria */
    mdlLocate_noElemNoLocked ();
    /* Set search mask to look for text and line elements */
    mdlLocate_setElemSearchMask (sizeof(searchType)/sizeof(int), searchType);
}

/*-----------------------------------------------------------------+
|                                                                  |
| Utility routines                                                 |
|                                                                  |
+-----------------------------------------------------------------*/
/*-----------------------------------------------------------------+
|                                                                  |
| name basic_errorPrint -- print an error message into Dialog      |
| Box Manager Messages dialog box                                  |
|                                                                  |
| author BSI 12/90                                                 |
|                                                                  |
+-----------------------------------------------------------------*/
Private void basic_errorPrint
(
    int errorNumber /* => number of error to print */
)
{
    char errorMsg[80];
    if (mdlResource_loadFromStringList(errorMsg, NULL, MESSAGELISTID_BasicErrors, errorNumber))
        return; /* unable to find msg. w/ num. "errorNumber" */
    mdlDialog_dmsgsPrint(errorMsg);
}