/*-----------------------------------------------------------------+
|                                                                  |
| Constants & types used in basic dialog example                   |
|                                                                  |
+-----------------------------------------------------------------*/
#ifndef __basicH__
#define __basicH__
/*-----------------------------------------------------------------+
|                                                                  |
| Resource ID's                                                    |
|                                                                  |
+-----------------------------------------------------------------*/
#define DIALOGID_Basic 1            /* dialog id for Basic Dialog */
#define DIALOGID_BasicModal 2       /* dialog id for Basic Modal Dialog */
#define OPTIONBUTTONID_Basic 1      /* id for parameter 1 option button */
#define PUSHBUTTONID_OModal 1       /* id for "Open Modal" push button */
#define PUSHBUTTONID_XDataShow 2    /* id for "XData Show" push button */
#define TEXTID_Basic 1              /* id for "parameter 1" text item */
#define TOGGLEID_Basic 1            /* id for "Inc parameter 1?" toggle */
#define SYNONYMID_Basic 1           /* id for synonym resource */
#define MESSAGELISTID_BasicErrors 1 /* id for error message list */
#define MESSAGELISTID_Commands 2    /* id for command message list */
#define MESSAGELISTID_Prompts 3     /* id for prompt message list */
/*-----------------------------------------------------------------+
|                                                                  |
| Message ID Definitions                                     |
|                                                                  |
+-----------------------------------------------------------------*/
#define ERRID_CommandTable 1
#define ERRID_BasicDialog 2
#define ERRID_ModalDialog 3

#define COMMANDID_XDataShow 1

#define PROMPTID_IdentifyElement 1 
/*-----------------------------------------------------------------+
|                                                                  |
| Hook Function ID's                                               |
|                                                                  |
+-----------------------------------------------------------------*/
#define HOOKITEMID_ToggleButton_Basic 1 /* toggle for item hook id */
#define HOOKDIALOGID_Basic 2            /* id for dialog hook func */
/*-----------------------------------------------------------------+
|                                                                  |
| Typedefs                                                         |
|                                                                  |
+-----------------------------------------------------------------*/
typedef struct basicglobals
{
    int parameter1; /* used by text & option button item */
    int parameter2; /* used by toggle button item */
} BasicGlobals;
/*-----------------------------------------------------------------+
|                                                                  |
| Text string resources                                            |
|                                                                  |
+-----------------------------------------------------------------*/
#define TXT_BasicDialogBox "Read ASD XData Example"
#define TXT_BasicModalDialogBox "Basic Modal Dialog Box"
#define TXT_Parameter1 "Parameter 1:"
#define TXT_OpenModal "Open Modal"
#define TXT_XDataShow "Show XData"
#define TXT_IncrementParameter1 "Increment parameter 1?"
#define TXT_Value1 "Value 1"
#define TXT_Value2 "Value 2"
#define TXT_Value3 "Value 3"

#endif