/*----------------------------------------------------------------+
| |
| Function - |
| |
| "Basic Dialog Example" Command Table Resources |
| |
+----------------------------------------------------------------*/
/*----------------------------------------------------------------+
| |
| Include Files |
| |
+----------------------------------------------------------------*/
#include <rscdefs.h>
#include <cmdclass.h>
/*----------------------------------------------------------------+
| |
| Local Defines |
| |
+----------------------------------------------------------------*/
#define CT_NONE 0
#define CT_ASDXDATA 1
#define CT_ACTIONS 2
/*----------------------------------------------------------------+
| |
| "Basic dialog example" commands |
| |
+----------------------------------------------------------------*/
Table CT_ASDXDATA=
{
    {1, CT_ACTIONS, INPUT, REQ, "ASDXDATA"},
};

Table CT_ACTIONS=
{
    {1, CT_NONE, INPUT, REQ, "OPENMODAL"},
    {2, CT_NONE, MANIPULATION, NONE, "SHOW"},
};