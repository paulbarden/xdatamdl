/*----------------------------------------------------------------+
| |
| Function - |
| |
| Basic application message string resources |
| |
+----------------------------------------------------------------*/
#include <dlogbox.h>    /* dlog box manager resource consts & structs */
#include <dlogids.h>    /* MicroStation resource IDs */
#include "XdataMdl.h"   /* basic dialog box example consts & structs */
/*----------------------------------------------------------------+
| |
| Error Messages |
| |
+----------------------------------------------------------------*/
MessageList MESSAGELISTID_BasicErrors=
{
    {
        {ERRID_CommandTable, "Unable to load command table."},
        {ERRID_BasicDialog, "Unable to open Basic dialog box."},
        {ERRID_ModalDialog, "Unable to open modal dialog box."}
    }
};